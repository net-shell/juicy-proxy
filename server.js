var http = require('http');
var DataNode = require('kosyak');
var proxy = require('redbird')({
	port: 6665,
	xfwd: false,
	cluster: 1,
	resolvers: [resolveRequest]
});

var masterDataNodeConfig = {
  host: '127.0.0.1',
  port: 13390
};

var firstNode = new DataNode(masterDataNodeConfig).init(dataNodeErrorCallback);
var secondNode = new DataNode({ 'master': masterDataNodeConfig }).init(dataNodeErrorCallback);

var dataNodeErrorCallback = function(err) { }

var resolveRequest = function(host, url) {
	var key = 'prx_' + host;
	var val = firstNode.get(key);
	console.log(val)
	var target = {
		ip: '127.0.0.1',
		port: 8888,
		protocol: 'http',
		path: '/'
	};
	return target.protocol + '://' + target.ip + ':' + target.port + target.path;
}
// resolveRequest.priority = 0

var app = http.createServer(function(req,res){
    res.send('secured content');
});
app.listen(8888);